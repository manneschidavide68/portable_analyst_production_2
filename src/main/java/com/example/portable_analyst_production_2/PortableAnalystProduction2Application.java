package com.example.portable_analyst_production_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortableAnalystProduction2Application {

    public static void main(String[] args) {
        SpringApplication.run(PortableAnalystProduction2Application.class, args);
    }

}
